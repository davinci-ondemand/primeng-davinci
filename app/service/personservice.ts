import {Person} from "../domain/person";
import "rxjs/Rx";
import {Http} from "@angular/http";
import {Injectable} from "@angular/core";

@Injectable()
export class PersonService {

    constructor(private http: Http) {
    }

    getPersons() {
        return this.http.get('app/resources/persons.json')
            .toPromise()
            .then(res => <Person[]> res.json().data)
            .then(data => {
                return data;
            });
    }

}