import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {BrowserModule} from "@angular/platform-browser";
import "rxjs/add/operator/toPromise";
import {AppComponent} from "./app.component";
import {PersonService} from "./service/personservice";
import {InputTextModule, DataTableModule, ButtonModule, DialogModule} from "primeng/primeng";

@NgModule({
    imports: [BrowserModule, FormsModule, HttpModule, InputTextModule, DataTableModule, ButtonModule, DialogModule],
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    providers: [PersonService]
})
export class AppModule {
}